import 'package:flutter/material.dart';
import 'package:twilio_flutter/twilio_flutter.dart';

class MyHomeScreen extends StatefulWidget {
  MyHomeScreen({Key? key, required this.title, required this.phoneNumber})
      : super(key: key);
  final String title;
  final String phoneNumber;
  @override
  _MyHomeScreenState createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  late TwilioFlutter twilioFlutter;
  @override
  void initState() {
    twilioFlutter = TwilioFlutter(
        accountSid: 'AC9562deda191c3dfe6907651a326df6bc',
        authToken: '5f66167796da2d12a6af39057c5c4c80',
        twilioNumber: '+19034209837');
    super.initState();
  }

  void sendSms() async {
    twilioFlutter.sendSMS(
        toNumber: widget.phoneNumber,
        messageBody: 'Hii everyone this is a demo of\nflutter twilio sms.');
  }

  void getSms() async {
    var data = await twilioFlutter.getSmsList();
    print(data);
    await twilioFlutter.getSMS('***************************');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                Color(0xFFE1F5FE),
                Color(0xFFB3E5FC),
              ],
              begin: FractionalOffset(0.0, 0.0),
              end: FractionalOffset(1.0, 0.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 200,
            ),
            Text(
              'Press the button to send SMS.',
              style: TextStyle(color: Colors.blue, fontSize: 20),
            ),
            SizedBox(
              height: 100,
            ),
            Align(
              alignment: Alignment.center,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: CircleBorder(),
                    primary: Colors.blue, // background
                    onPrimary: Colors.white,
                    minimumSize: Size(150, 150) // foreground
                    ),
                onPressed: () {},
                child: Text(
                  'S.O.S',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        backgroundColor: Colors.white,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,

          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.brown.shade800,
                    minRadius: 30,
                    child: const Text(
                      'Photo',
                    ),
                  ),
                  Divider(),
                  Text(
                    'Name',
                    style: TextStyle(color: Colors.blue, fontSize: 20),
                  ),
                ],
              ),
            ),
            Divider(),
            ListTile(
              title: const Text(
                'Account',
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            Divider(
              color: Colors.lightGreen,
            ),
            ListTile(
              title: const Text(
                'Settings',
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            Divider(
              color: Colors.lightGreen,
            ),
            ListTile(
              title: const Text(
                'Log out',
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            Divider(
              color: Colors.lightGreen,
            ),
            ListTile(
              title: const Text(
                'About us',
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            Divider(
              color: Colors.lightGreen,
            ),
          ],
        ),
      ),
    );
  }
}
